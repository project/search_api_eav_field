Provides the integration of EAV fields (https://www.drupal.org/project/eav_field) with the Search API and facets.

The EAV Field processor indexes each EAV attribute as a separate field.

"Transform entity ID to label" facets processor renders the entity label instead of its identifier (for example, a term name instead of a taxonomy term identifier).
