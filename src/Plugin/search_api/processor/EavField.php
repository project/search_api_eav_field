<?php

namespace Drupal\search_api_eav_field\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Processor\ProcessorProperty;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\search_api\Item\ItemInterface;

/**
 * Adds the item's eav fields to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "eav_field",
 *   label = @Translation("EAV Field"),
 *   description = @Translation("Adds the item's eav fields to the indexed data."),
 *   stages = {
 *     "add_properties" = 0,
 *     "pre_index_save" = -10,
 *   },
 * )
 */
class EavField extends ProcessorPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $eavAttributeStorage;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $eavValueStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $processor->eavAttributeStorage = $container->get('entity_type.manager')->getStorage('eav_attribute');
    $processor->eavValueStorage = $container->get('entity_type.manager')->getStorage('eav_value');

    return $processor;
  }
  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $eav_fields = $this->eavAttributeStorage->loadMultiple();
      foreach ($eav_fields as $eav_field) {
        // Get settings for the current eav attribute.
        $eav_field_config = $eav_field->getValueStorageConfigArray();

        // Determine type key.
        $eav_type = $eav_field_config['type'];
        switch ($eav_type) {
          case 'integer':
          case 'list_integer':
          case 'entity_reference':
            $type = 'integer';
            break;
          case 'decimal':
            $type = 'decimal';
            break;
          case 'boolean':
            $type = 'boolean';
            break;
          default:
            $type = 'string';
        }

        // Determine is_list key.
        if ($eav_field_config['cardinality'] == '1') {
          $is_list = FALSE;
        } else {
          $is_list = TRUE;
        }

        $definition = [
          'label' => $eav_field->label(),
          'type' => $type,
          'processor_id' => $this->getPluginId(),
          'is_list' => $is_list,
          'hidden' => TRUE,
          'settings' => $eav_field_config,
        ];

        $properties['eav_field_' . $eav_field->id()] = new ProcessorProperty($definition);
      }
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preIndexSave() {
    // @TODO Need to add new fields whenever a new attribute is added
    $eav_fields = $this->eavAttributeStorage->loadMultiple();
    foreach ($eav_fields as $eav_field) {
      $field = $this->ensureField(NULL, 'eav_field_' . $eav_field->id());
      $field->setHidden();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity = $item->getOriginalObject();
    $fields = $item->getFields();

    // Retrieve all EavValue ids for current entity.
    $eav_values = [];
    foreach ($entity->getProperties() as $property) {
      if ($property->getDataDefinition()->getType() == 'eav') {
        $target_ids = array_column($property->getValue(), 'target_id');
        $eav_values = array_merge($eav_values, array_values($target_ids));
      }
    }

    // Maybe someday will duplicate values if values will reuseful.
    $eav_values = array_unique($eav_values);

    $eav_values = $this->eavValueStorage->loadMultiple($eav_values);
    foreach ($eav_values as $eav_value) {
      $field_value = $eav_value->getValueFieldValue();
      if ($field_value) {
        $eav_attribute_id = $eav_value->getAttributeId();
        if (isset($fields['eav_field_' . $eav_attribute_id])) {
          /** @var \Drupal\search_api\Item\FieldInterface $field */
          $field = $fields['eav_field_' . $eav_attribute_id];
          $eav_attribute_type = $eav_value->getAttributeEntity()->getValueType();
          // We get data for the field depending on the type of the attribute.
          switch ($eav_attribute_type) {
            case 'entity_reference':
              $values = array_column($field_value, 'target_id');
              break;
            // integer, decimal, string, boolean, list_integer, list_string.
            default:
              $values = array_column($field_value, 'value');
          }
          // Add values to the current field.
          foreach ($values as $value) {
            $field->addValue($value);
          }
        }
      }
    }

  }

}
